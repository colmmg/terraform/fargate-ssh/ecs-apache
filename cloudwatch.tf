resource "aws_cloudwatch_log_group" "ecs" {
  name              = "/aws/ecs/apache"
  retention_in_days = "30"
  tags = {
    Name = "/aws/ecs/apache"
  }
}
