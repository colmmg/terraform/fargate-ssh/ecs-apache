terraform {
  backend "s3" {
    bucket = "tf-state-us-east-1-123456789012"
    key    = "ecs-apache/terraform.tfstate"
    region = "us-east-1"
  }
}
