data "template_file" "container-definitions" {
  template = file("${path.module}/templates/container-definitions.json.tpl")
  vars = {
    account-id     = data.aws_caller_identity.current.account_id
    ecs-public-key = data.terraform_remote_state.ecs.outputs.ecs-public-key
    region         = var.region
  }
}

resource "aws_ecs_task_definition" "ecs" {
  family                   = "apache"
  container_definitions    = data.template_file.container-definitions.rendered
  execution_role_arn       = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/ecsTaskExecutionRole"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "512"
  memory                   = "1024"
}

resource "aws_ecs_cluster" "ecs" {
  name = "apache"
  tags = {
    Name = "apache" 
  }
}

resource "aws_service_discovery_service" "apache" {
  name = "apache"
  dns_config {
    namespace_id = data.terraform_remote_state.ecs.outputs.private-namespace-id
    dns_records {
      ttl  = 10
      type = "A"
    }
    routing_policy = "MULTIVALUE"
  }
  health_check_custom_config {
    failure_threshold = 1
  }
}

resource "aws_default_subnet" "a" {
  availability_zone = "${var.region}a"
}

resource "aws_ecs_service" "apache" {
  name                              = "apache"
  cluster                           = aws_ecs_cluster.ecs.id
  task_definition                   = aws_ecs_task_definition.ecs.arn
  desired_count                     = "1"
  launch_type                       = "FARGATE"
  network_configuration {
    assign_public_ip = "true"
    security_groups  = [data.terraform_remote_state.ecs.outputs.ecs-web-sg-id]
    subnets          = [aws_default_subnet.a.id]
  }
  service_registries {
    registry_arn = aws_service_discovery_service.apache.arn
  }
}
