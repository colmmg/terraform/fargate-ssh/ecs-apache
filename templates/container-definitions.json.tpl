[
  {
    "name": "apache",
    "image": "${account-id}.dkr.ecr.${region}.amazonaws.com/apache:latest",
    "environment": [
      {
        "name": "SSH_PUBLIC_KEY",
        "value": "${ecs-public-key}"
      }
    ],
    "essential": true,
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
            "awslogs-group": "/aws/ecs/apache",
            "awslogs-region": "${region}",
            "awslogs-stream-prefix": "apache"
        }
    },
    "portMappings": [
      {
        "hostPort": 22,
        "protocol": "tcp",
        "containerPort": 22
      },
      {
        "hostPort": 80,
        "protocol": "tcp",
        "containerPort": 80
      }
    ]
  }
]
