provider "aws" {
  region = var.region
}

data "aws_caller_identity" "current" {}

data "terraform_remote_state" "ecs" {
  backend = "s3"
  config = {
    bucket = "tf-state-${var.region}-${data.aws_caller_identity.current.account_id}"
    key    = "infra/ecs/terraform.tfstate"
    region = var.region
  }
}
